<?php
namespace d3x\Vasco;

use d3x\starter\StarterServiceProvider;
use Illuminate\Support\ServiceProvider;

//use wpm\furs\Traits\PublishesMigrations;
class VascoServiceProvider extends ServiceProvider
{
//    use PublishesMigrations;
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/config/vasco.php' => config_path('vasco.php'),
        ]);
    }

    public function register()
    {
        $this->app->register(StarterServiceProvider::class);
        $this->mergeConfigFrom(
            __DIR__ . '/config/vasco.php', 'vasco'
        );
    }
}
