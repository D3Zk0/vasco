<?php

namespace d3x\Vasco\API\Requests;

use Illuminate\Support\Facades\Config;

class Partner extends VascoRequest
{
    public function __construct($method)
    {
        $this->action = "/api/v1/SkupniSifranti/partner";
        $this->method = $method;
        $this->api_key = Avtentikacija::getApiKey();
        parent::__construct();
    }

    public static function get($options = [])
    {
        $client = (new self("GET"));
        $client->postfields = $options;
        $client->call();
        return collect($client->response);
    }

    public static function create()
    {
        $client = (new self("GET"));

    }

    public static function update()
    {
        $client = (new self("GET"));

    }

    public static function delete()
    {
        $client = (new self("GET"));

    }

    public static function modelPointers()
    {
        return [
            "sifra" => 1,
            "naziv" => "name",
            "naziv2" => 1,
            "naslov" => "address",
            "posta" => "zip",
            "drzava" => "cou",
            "davcniZavezanec" => 1,
            "ident" => 1,
            "tip" => 1,
            "maticna" => 1,
            "telefon" => 1,
            "telefaks" => 1,
            "gsm" => 1,
            "dodatnaPolja" => 1,
            "trr" => 1,
            "prodajalne" => 1,
        ];
    }

}
