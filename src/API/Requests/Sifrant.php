<?php

namespace d3x\Vasco\API\Requests;

class Sifrant extends VascoRequest
{
    public function __construct($method)
    {
        $this->action = "/api/v1/FASifranti/";
        $this->method = $method;
        $this->api_key = Avtentikacija::getApiKey();
        parent::__construct();
    }

    public static function get($type, $options = [])
    {
        $client = (new self("GET"));
        $client->postfields = $options;
        $client->action .= $type;
        $client->call();
        return collect($client->response);
    }

}
