<?php

namespace d3x\Vasco\API\Requests;

class NarociloKupca extends VascoRequest
{
    public function __construct($method)
    {
        $this->action = "/api/v1/FA/narociloKupca";
        $this->method = $method;
        $this->api_key = Avtentikacija::getApiKey();
        parent::__construct();
    }

    public static function get($options = [])
    {
        $client = (new self("GET"));
        $client->postfields = $options;
        $client->call();
        return collect($client->response);
    }

    public static function create($data = [])
    {
        $client = (new self("POST"));
        $client->postfields = $data;
        $client->call();
        return collect($client->response);
    }

    public static function update($data = [])
    {
        $client = (new self("PUT"));
        $client->postfields = $data;
        $client->call();
        return collect($client->response);
    }

    public static function items()
    {
        $client = (new self("PUT"));
        $client->action = "/api/v1/FA/narociloKupca/postavke";
        $client->call();
        return collect($client->response);
    }

    public static function status($number, $year)
    {
        $client = (new self("GET"));
        $client->action = "/api/v1/FA/narociloKupca/stanje/{$number}/{$year}";
        $client->call();
        return collect($client->response);
    }

    public static function fields()
    {
        $client = (new self("GET"));
        $client->action = "/api/v1/FA/narociloKupca/polja";
        $client->call();
        return collect($client->response);
    }

    public static function itemFields()
    {
        $client = (new self("GET"));
        $client->action = "/api/v1/FA/narociloKupca/postavke/polja";
        $client->call();
        return collect($client->response);
    }
}
