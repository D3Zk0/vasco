<?php

namespace d3x\Vasco\API\Requests;

class Zaloga extends VascoRequest
{
    public function __construct($method)
    {
        $this->action = "/api/v1/FASifranti/zaloga";
        $this->method = $method;
        $this->api_key = Avtentikacija::getApiKey();
        parent::__construct();
    }

    public static function get($options = [])
    {
        $client = (new self("GET"));
        $client->postfields = $options;
        $client->call();
        return collect($client->response);
    }

    public static function create()
    {
        $client = (new self("GET"));

    }

    public static function update()
    {
        $client = (new self("GET"));

    }

    public static function delete()
    {
        $client = (new self("GET"));

    }

}
