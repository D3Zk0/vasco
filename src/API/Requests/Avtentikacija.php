<?php

namespace d3x\Vasco\API\Requests;

class Avtentikacija extends VascoRequest
{
    public function __construct()
    {
        parent::__construct();
        $this->action = "/api/v1/Avtentikacija";
        $this->method = "POST";
        $this->postfields = $this->generateData();
        $this->call();
    }

    public function generateData()
    {
        return [
            "username" => $this->username,
            "password" => $this->password,
            "taxNumber" => $this->tax_number,
            "year" => 0,
            "returnPastYears" => true,
            "params" => [
                "additionalProp1" => "string",
                "additionalProp2" => "string",
                "additionalProp3" => "string"
            ]
        ];
    }

    public static function getApiKey()
    {
        return (new self)->response->apiKey;
    }

}
