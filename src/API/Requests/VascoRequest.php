<?php

namespace d3x\Vasco\API\Requests;

use d3x\Vasco\API\Exceptions\CurlException;
use d3x\Vasco\API\Exceptions\DPDException;
use d3x\Vasco\API\Exceptions\VascoException;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;

abstract class VascoRequest
{
    public $action;
    public $method;
    public $response;

    protected $url;
    protected $username;
    protected $password;
    protected $tax_number;

    protected $api_key;
    protected $postfields = null;

    protected const HEADERS = [
        'accept: application/json',
        'Content-Type: application/json'
    ];

    public function __construct()
    {
        $this->url = Config::get('vasco.connection.url');
        $this->username = Config::get('vasco.connection.username');
        $this->password = Config::get('vasco.connection.password');
        $this->tax_number = Config::get('vasco.connection.tax_number');
    }

    public function getPostFields()
    {
        return [];
    }

    public function getHeaders()
    {
        $headers = self::HEADERS;
        if ($this->api_key)
            $headers[] = "Authorization: Bearer " . $this->api_key;
        return $headers;
    }

    public function getOptions()
    {
        $options = [
            CURLOPT_URL => $this->url . $this->action,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $this->method,
            CURLOPT_HTTPHEADER => $this->getHeaders(),
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
        ];
        if (in_array($this->method, ['GET', 'get']) && $this->postfields) {
            $query_string = http_build_query($this->postfields);
            if (!empty($query_string))
                $options[CURLOPT_URL] .= '?' . $query_string;
        }
        if ($this->postfields)
            $options[CURLOPT_POSTFIELDS] = json_encode($this->postfields);
        return $options;
    }

    public function call()
    {
        $curl = curl_init();
        $options = $this->getOptions();

        curl_setopt_array($curl, $options);
        $response = curl_exec($curl);
        if (curl_errno($curl)) {
            throw new CurlException($curl);
        } else {
            $http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            $options["response_code"] = $http_code;
            if (!in_array($http_code, [200, 201, 202, 204, 206]))
                throw new VascoException($options, $response);
        }
        curl_close($curl);
        $this->response = json_decode($response);
    }

}
