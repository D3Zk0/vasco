<?php

namespace d3x\Vasco\API\Requests;

class Skladisce extends VascoRequest
{
    public function __construct($method)
    {
        $this->action = "/api/v1/FASifranti/skladisca";
        $this->method = $method;
        $this->api_key = Avtentikacija::getApiKey();
        parent::__construct();
    }

    public static function get($options = [])
    {
        $client = (new self("GET"));
        $client->postfields = $options;
        $client->call();
        return collect($client->response);
    }

}
