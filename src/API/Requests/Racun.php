<?php

namespace d3x\Vasco\API\Requests;

class Racun extends VascoRequest
{
    public function __construct($method)
    {
        $this->action = "/api/v1/FA/racun";
        $this->method = $method;
        $this->api_key = Avtentikacija::getApiKey();
        parent::__construct();
    }


    public static function get($options)
    {
        $client = (new self("GET"));
        $client->postfields = $options;
        $client->call();
        return $client->response;
    }

    public static function create()
    {
        $client = (new self("GET"));

    }

    public static function update()
    {
        $client = (new self("GET"));

    }

    public static function delete()
    {
        $client = (new self("GET"));

    }

    public static function content($number, $year)
    {
        $client = (new self("GET"));
        $client->action = "/api/v1/FA/racun/vsebina/{$number}/{$year}";
        $client->call();
        return collect($client->response);
    }

}
