<?php

namespace d3x\Vasco\API\Requests;

class Dobavnica extends VascoRequest
{
    public function __construct($method)
    {
        $this->action = "/api/v1/FA/dobavnica";
        $this->method = $method;
        $this->api_key = Avtentikacija::getApiKey();
        parent::__construct();
    }


    public static function get($options)
    {
        $client = (new self("GET"));
        $client->postfields = $options;
        $client->call();
        return $client->response;
    }

    public static function create($data)
    {
        $client = (new self("POST"));
        $client->postfields = $data;
        $client->call();
        return collect($client->response);
    }

    public static function facture($stevilka, $leto)
    {
        $client = (new self("POST"));
        $client->action = "/api/v1/FA/dobavnica/fakturiranje";
        $client->postfields = compact("stevilka", "leto");
        $client->call();
        return collect($client->response);
    }

    public static function fields()
    {
        $client = (new self("GET"));
        $client->action = "/api/v1/FA/dobavnica/polja";
        $client->call();
        return collect($client->response);
    }

    public static function itemFields()
    {
        $client = (new self("GET"));
        $client->action = "/api/v1/FA/dobavnica/postavka/polja";
        $client->call();
        return collect($client->response);
    }

    public static function content($number, $year)
    {
        $client = (new self("GET"));
        $client->action = "/api/v1/FA/dobavnica/vsebina/{$number}/{$year}";
        $client->call();
        return collect($client->response);
    }




}
