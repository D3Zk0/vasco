<?php

return [
    "connection" => [
        "username" => env("VASCO_USERNAME", ""),
        "password" => env("VASCO_PASSWORD", ""),
        "tax_number" => env("VASCO_TAX_NUMBER", ""),
        "url" => env("VASCO_URL", ""),
    ],

];
